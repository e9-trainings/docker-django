=======
Selvano
=======

Getting started
---------------

Run in the root directory::

    $ docker-compose up
    $ docker-compose run django python django-selvano/manage.py migrate

The app will be running at http://localhost:8000

Happy coding!
